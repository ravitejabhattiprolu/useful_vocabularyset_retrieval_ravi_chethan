/**
 * @AUTHOR1 SRI RAMA RAVI TEJA BHATTIPROLU
 * @AUTHOR2 CHETHAN MIRIYALA
 * @Project2
 */
package usefulvocabularysetretrieval;

import java.util.*;
import java.io.*;

public class UsefulVocabularySetRetrieval {

    static File dataSetFile = new File("src/dataset");
    static Map<String, List<String>> map = new HashMap<>();

    public static void main(String[] args) throws FileNotFoundException, IOException {
        try {
            if (readDataSet() != -1) {
                int numOfWords = 0;
                List<String> words = new ArrayList<>();
                Map<String, Double> tf = new HashMap<>();
                Map<String, Double> idf = new HashMap<>();
                Map<String, Double> tfidf = new HashMap<>();
                System.out.println("How many words you want to specify in the vocabulary set?");
                Scanner sc = new Scanner(System.in);
                numOfWords = sc.nextInt();
                if (numOfWords > 0) {
                    System.out.println("\nPlease enter the words in the vocabulary set");
                    for (int i = 1; i <= numOfWords; i++) {
                        sc = new Scanner(System.in);
                        words.add(sc.nextLine());
                    }
                    for (Iterator<String> iter = words.iterator(); iter.hasNext();) {
                        String word = iter.next();
                        double temptf = calculateTermFrequency(word);
                        double tempidf = calculateInverseDocumentFrequency(word);
                        double temptfidf = Math.round(temptf * tempidf * 100.0) / 100.0;
                        tf.put(word, temptf);
                        idf.put(word, tempidf);
                        tfidf.put(word, temptfidf);
                    }
                    System.out.println("\nTerm frequency list:\n" + sortByFrequencyValue(tf));
                    System.out.println("\nInverse document frequency list:\n" + sortByFrequencyValue(idf));
                    System.out.println("\nTerm frequency - Inverse document frequency weight list:\n" + sortByFrequencyValue(tfidf));
                    printUsefulVocabularySet(sortByFrequencyValue(tfidf));
                } else {
                    System.out.println("Number of words in a vocabulary set cannot be empty");
                }
            }
        } catch (Exception ex) {
            System.out.println("Please input valid data and try again");
        }
    }

    public static <K, V extends Comparable<? super V>> Map<K, V> sortByFrequencyValue(Map<K, V> map) {
        List<Map.Entry<K, V>> list =
                new LinkedList<Map.Entry<K, V>>(map.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
            public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
                return (o2.getValue()).compareTo(o1.getValue());
            }
        });

        Map<K, V> result = new LinkedHashMap<K, V>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }

    public static void printUsefulVocabularySet(Map<String, Double> map) {
        double harmonicMean = calculateHarmonicMean(map);
        List<String> finalWords = new ArrayList<>();
        if (harmonicMean > 0) {
            for (Iterator<Map.Entry<String, Double>> mapIterator = map.entrySet().iterator(); mapIterator.hasNext();) {
                Map.Entry<String, Double> currentMapEntry = mapIterator.next();
                if (currentMapEntry.getValue() >= harmonicMean) {
                    finalWords.add(currentMapEntry.getKey());
                }
            }
        }
        if (finalWords.size() > 0) {
            Collections.sort(finalWords);
            System.out.println("\nVocabulary set most important for ranking:");
            System.out.println(finalWords);
        } else {
            System.out.println("\nVocabulary set provided is not important for ranking");
        }
    }

    public static double calculateHarmonicMean(Map<String, Double> map) {
        double harmonicMean = 0, tempHarmonicMean;
        int count = 0;
        for (Iterator<Map.Entry<String, Double>> mapIterator = map.entrySet().iterator(); mapIterator.hasNext();) {
            Map.Entry<String, Double> currentMapEntry = mapIterator.next();
            tempHarmonicMean = currentMapEntry.getValue();
            if (tempHarmonicMean > 0) {
                harmonicMean = harmonicMean + (1 / tempHarmonicMean);
                count++;
            }
        }
        if (harmonicMean > 0) {
            harmonicMean = count / harmonicMean;
        }
        return harmonicMean;
    }

    public static double calculateTermFrequency(String term) {
        double termFrequency = 0;
        for (Iterator<Map.Entry<String, List<String>>> mapIterator = map.entrySet().iterator(); mapIterator.hasNext();) {
            Map.Entry<String, List<String>> currentMapEntry = mapIterator.next();
            termFrequency = termFrequency + Collections.frequency(currentMapEntry.getValue(), term);
        }
        if (termFrequency != 0) {
            termFrequency = 1 + (Math.log(termFrequency) / Math.log(2));
            termFrequency = Math.round(termFrequency * 100.0) / 100.0;
        }
        return termFrequency;
    }

    public static double calculateInverseDocumentFrequency(String term) {
        double inverseDocumentFrequency = 0;
        int frequencyCount = 0;
        for (Iterator<Map.Entry<String, List<String>>> mapIterator = map.entrySet().iterator(); mapIterator.hasNext();) {
            Map.Entry<String, List<String>> currentMapEntry = mapIterator.next();
            frequencyCount = Collections.frequency(currentMapEntry.getValue(), term);
            if (frequencyCount != 0) {
                inverseDocumentFrequency++;
            }
        }
        if (inverseDocumentFrequency != 0) {
            inverseDocumentFrequency = Math.log((map.size() / inverseDocumentFrequency)) / Math.log(2);
            inverseDocumentFrequency = Math.round(inverseDocumentFrequency * 100.0) / 100.0;
        }
        return inverseDocumentFrequency;
    }

    public static int readDataSet() throws FileNotFoundException, IOException {
        try (Scanner sc = new Scanner(new FileReader(dataSetFile.getPath()))) {
            String tempString, tempKey;
            List<String> tempArrayList = null, splitWords = null, splitKey = null;
            while (sc.hasNext()) {
                tempString = sc.nextLine();
                splitKey = Arrays.asList(tempString.split("\\s+"));
                tempKey = splitKey.get(0);
                splitWords = splitKey.subList(1, splitKey.size());
                tempArrayList = new ArrayList<>();
                if (map.containsKey(tempKey)) {
                    List<String> value = map.get(tempKey);
                    value.addAll(splitWords);
                    map.put(tempKey, value);
                } else {
                    tempArrayList.addAll(splitWords);
                    map.put(tempKey, tempArrayList);
                }
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Data set not found...");
            return -1;
        } catch (Exception ex) {
            System.out.println("Something went wrong. Please try again with valid data set...");
            return -1;
        }
        return 0;
    }
}